module maths

implicit none

integer, parameter :: dp = selected_real_kind(15)
integer, parameter :: di = selected_real_kind(15)

real(dp), parameter :: pi = 3.14159265359d0


contains


! Input or generate the random seed based on system clock
subroutine random_initial(seed_input)

    implicit none

    integer, intent(in) :: seed_input
    integer, allocatable, dimension(:) :: seed
    integer :: seed_size, j

    ! Get seed dimension
    call random_seed(size = seed_size)
    allocate(seed(seed_size))

    ! Check if using inputted seed or seed based on system clock
    if (seed_input == 0) then

        ! Seed using the system clock
        do j = 1, size(seed)

            call system_clock(count = seed(j))
            seed(j) = seed(j) * j

        end do

    else

        ! Use inputted seed
        seed = seed_input

    end if

    ! Assign the seed_size
    call random_seed(put = seed)


end subroutine


! Use the Box-Muller transform to calculate 2 random numbers from a normal distribution
subroutine random_normal(number1, number2, variance, mean)

    implicit none

    real(dp), intent(in) :: variance, mean
    real(dp), intent(out) :: number1, number2
    real(dp) :: x, y, r

    do

        ! Calculate 2 uniform random numbers from 0 to 1
        call random_number(x)
        call random_number(y)

        ! Check if uniform numbers are suitable for Box-Muller transform
        x = (2.d0 * x) - 1.d0
        y = (2.d0 * y) - 1.d0

        r = (x*x) + (y*y)

        if (r < 1.d0) exit

    end do

    ! Perform the Box-Muller transform
    r = -2.d0 * variance * log(r) / r
    r = sqrt(r)

    number1 = mean + (x*r)
    number2 = mean + (y*r)

end subroutine


end module
