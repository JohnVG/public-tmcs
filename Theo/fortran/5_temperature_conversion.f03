program temperature_conversion

use maths

implicit none
real(dp) :: a, b, C, F
integer :: j

! Calculate constants
a = 9.d0 * 0.2d0
b = 32.d0

! Convert individual temperature
write(*,*) "Input temperature in celsuis:"
read(*,*) C

call farenheit(a, b, C, F)

write(*,*) "Temperature in farenheit:", F
write(*,*) ""


! Produce a conversion table
write(*,*) "Celsius | Farenheit"

do j = 0, 100

    C = j * 1.d0

    call farenheit(a, b, C, F)

    write(*,*) C, F

end do


contains

subroutine farenheit(a,b,C,F)

    implicit none
    real(dp), intent(in) :: a,b,C
    real(dp), intent(out) :: F

    F = (a*C) + b

end subroutine

end program
