program mass_spectrometry_unknowns

implicit none
integer :: max_C, j
real, dimension(4) :: target_M
real :: tolerance


! Target masses
target_M(1) = 103.08
target_M(2) = 152.11
target_M(3) = 152.10
target_M(4) = 153.09

! Tolerance of correct answer set to precision of target masses
tolerance = 0.01

! Maximum number of C atoms
max_C = 9

write(*,*) "Output in form: C,N,H,O"

! Main
do j = 1,4
    write(*,*) "Molecule Mass =", target_M(j)
    call main(target_M(j), tolerance, max_C)
    write(*,*)
end do

contains

! Calculate the total mass of the molecule
subroutine total_mass(C, N, O, H, M)

    implicit none
    integer, intent(in) :: C, N, O, H
    real, intent(out) :: M
    real, parameter :: M_C = 12.0038, M_N = 14.0075, M_O = 16.0000, M_H = 1.0081

    M = (C*M_C) + (N*M_N) + (O*M_O) + (H*M_H)

end subroutine


! Calculate the maximum number of H atoms according to standard valence rules
subroutine calculate_max_H(C, N, O, max_H)

    implicit none
    integer, intent(in) :: C, N, O
    integer, intent(out) :: max_H

    ! Number of H atoms for an alkane
    max_H = (2*C) + 2

    ! Subtract 1 H atom for each N atom
    max_H = max_H - N

    ! Subtract 2 H atoms for each O atom
    max_H = max_H - (2*O)

end subroutine


! Work out possible molecular forulae
subroutine main(target_M, tolerance, max_C)

    implicit none

    real, intent(in) :: target_M, tolerance
    integer, intent(in) :: max_C

    real :: M, difference
    integer :: C, N, O, H, max_H, O_plus_N



    ! Iterate through values of the total O+N
    do O_plus_N = 0, max_C

        ! Iterate through combinations of O and N up to the current total O+N
        do O = 0, O_plus_N

            N = O_plus_N - O

            ! Maximum number of C atoms is 9
            do C = 1, max_C

                call calculate_max_H(C, N, O, max_H)

                do H = 1, max_H

                    call total_mass(C, N, O, H, M)

                    ! Calculate difference between calculated mass and target mass
                    difference = M - target_M

                    ! Check to see if difference is below tolerance
                    if (abs(difference) < tolerance) then
                        write(*,*) C,N,H,O
                        exit

                    ! If over-estimating, cut the loop short
                    else if (difference > 0) then
                        exit

                    end if

                end do
            end do
        end do
    end do

end subroutine

end program
