program rotational_partition_function

use maths
implicit none

real(dp) :: cutoff, k, Bbeta, RPF, approx
real(dp), dimension(:), allocatable :: T, B
integer :: i, j, n_T, n_b
character (len=32), dimension(:), allocatable :: output


! Read in variables from input file
namelist /rot_pf1/ cutoff, n_b
namelist /rot_pf2/ B, output

open(101, file='input.in')
read(101, nml=rot_pf1)


! Define variables
! Boltzmann Constant
k = 1.38066d-23         ! in JK-1
k = k / 1.986d-23       ! in cm-1 K-1

! Temperature
n_T = 16
allocate(T(n_T))

do i = 1, n_T
    T(i) = (i*10.d0) - 5.d0   ! in K
end do

! Rotational Constants
allocate(B(n_b))
allocate(output(n_b))

read(101, nml=rot_pf2)
close(101)


! Sweep through B values
do i = 1, n_b

    ! Open output file
    open(102, file=output(i))

    ! Sweep through temperatures
    do j = 1, n_T

        ! Calculate constant used in calculation of partition function
        Bbeta = B(i) / (k * T(j))

        ! Calculate RPF at each temperature
        call calculate_RPF(Bbeta, cutoff, RPF)

        ! Calculate approximate RPF
        approx = T(j) / B(i)

        ! Output
        write(102,*) T(j), RPF, approx

    end do

    close(102)

end do


contains

! Calculates the rotational partition function
subroutine calculate_RPF(Bbeta, cutoff, RPF)

    implicit none
    real(dp), intent(in) :: Bbeta, cutoff
    real(dp), intent(out) :: RPF

    real(dp) :: term
    integer :: j

    ! j = 0 term
    j = 0
    RPF = 1.d0

    ! Remaining terms
    do

        ! Iterate to next value of j
        j = j+1

        ! Calculate the jth contribution
        term = (2.d0*j) + 1.d0
        term = term * exp( -Bbeta * j * (j+1) )

        RPF = RPF + term

        ! Exit if contributions become negligible
        if (term < (RPF*cutoff)) exit

    end do

end subroutine

end program
