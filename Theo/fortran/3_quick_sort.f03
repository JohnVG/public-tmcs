program quick_sort

implicit none
integer, dimension(100) :: list
integer :: j

! Read in list of random numbers
open(101,file="random_numbers.txt")

do j = 1, 100
    read(101,*) list(j)
end do

! Sort
call quick_sort_algorithm(list)

! Output the sorted list
do j = 1, 100
    write(*,*) list(j)
end do


contains

! Sort a single sub-list by one iteration of the quick sort
subroutine split(list, pivot_position)

    implicit none
    integer, dimension(:), intent(inout) :: list
    integer, intent(out) :: pivot_position

    integer :: n, pivot, j, x, n_less, n_more
    integer, dimension(:), allocatable :: less, more

    ! Allocate arrays
    n = size(list)
    allocate( less(n) )
    allocate( more(n) )

    ! Select a pivot value from the list
    pivot = list(1)

    ! Sweep through the remaining values in the list
    ! Sort list according to the pivot
    n_less = 1
    n_more = 1

    do j = 2, n

        x = list(j)

        if (x<pivot) then
            less(n_less) = x
            n_less = n_less + 1

        else
            more(n_more) = x
            n_more = n_more + 1

        end if

    end do

    ! Determine where the pivot is in the list
    pivot_position = n_less

    ! Create one output list
    do j = 1, pivot_position-1
        list(j) = less(j)
    end do

    list(pivot_position) = pivot

    do j = pivot_position+1, n
        list(j) = more(j-pivot_position)
    end do


    ! Deallocate temporary arrays
    deallocate(less)
    deallocate(more)

end subroutine


! Sort an entire list by several iterations of the quick sort algorithm
subroutine quick_sort_algorithm(list)

    implicit none
    integer, dimension(:), intent(inout) :: list

    integer :: n, n_work, i, j, k, new_pivot, n_pivots, a, b, difference
    integer, dimension(:), allocatable :: pivots, work_list
    logical :: escape


    n = size(list)


    ! Initiate range of list to extract working list from
    allocate( pivots(2) )
    pivots(1) = 0
    pivots(2) = n+1

    ! Set logical to enter outer loop
    escape = .false.

    do

        ! Extract working list from the main list
        a = pivots(1) + 1
        b = pivots(2) - 1

        n_work = b - a + 1
        allocate( work_list(n_work) )

        do j = a, b
            k = j - a + 1
            work_list(k) = list(j)
        end do


        ! Split the working list according to pivot
        call split(work_list, new_pivot)


        ! Reinsert working list
        do j = a, b
            k = j - a + 1
            list(j) = work_list(k)
        end do

        deallocate(work_list)


        ! Locate where the new pivot point is in the total list
        ! And insert it as the 2nd value of the pivots array
        n_pivots = size(pivots)

        allocate( work_list(n_pivots) )
        work_list = pivots

        deallocate(pivots)
        allocate( pivots(n_pivots+1) )

        pivots(1) = work_list(1)
        pivots(2) = pivots(1) + new_pivot

        do j = 2, n_pivots
            pivots(j+1) = work_list(j)
        end do

        deallocate(work_list)


        ! Check if next work list has reached minimum size
        ! If the work list to be extracted is only 1 element, it must already be sorted
        ! Loop is needed here, as the second working list could also be minimum size
        do

            ! Calculate size of the working list
            difference = pivots(2) - pivots(1)

            ! Clear the working list from the pivots list if below minimum size
            if (difference <= 2) then

                ! Remove pivots(1)
                n_pivots = size(pivots)-1

                allocate( work_list(n_pivots) )

                do j = 1, n_pivots
                    work_list(j) = pivots(j+1)
                end do

                deallocate(pivots)
                allocate( pivots(n_pivots) )
                pivots = work_list

                deallocate(work_list)


            ! If working list is still above minimum size, escape
            else
                exit

            end if


            ! Escape if the working list is of minimum size
            ! And the last possible working list
            if (n_pivots < 2) then
                escape = .true.
                exit
            end if

        end do

        if (escape) exit

    end do

end subroutine


end program
