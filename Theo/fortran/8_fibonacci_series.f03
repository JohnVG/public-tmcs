program fibonacci_series

implicit none
integer :: n, i
integer, dimension(:), allocatable :: series

n = 25
allocate( series(n) )

! Initialise Fibonacci Series
series(1) = 1
series(2) = 1

! Calculate terms up to the nth term
do i = 3, n
    series(i) = series(i-1) + series(i-2)
end do

write(*,*) series

end program
