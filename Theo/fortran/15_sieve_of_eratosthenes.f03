program sieve_of_eratosthenes

implicit none
integer :: n, i, j, base_prime, tester, new_list_count
integer, dimension(:), allocatable :: list, temp_list


! Generate a list of numbers up to n
n = 500
allocate(list(n-1))
allocate(temp_list(n-1))

do j = 2, n
    list(j-1) = j
end do


! Sweep through values in the list as the base prime number to test
do i = 1, n-1

    ! Get first base prime value
    base_prime = list(i)

    ! Exit condition
    if (n < (base_prime*base_prime)) exit

    ! Initialise
    tester = base_prime + base_prime
    new_list_count = 0


    ! Sweep through values in list
    do j = 1, size(list)

        ! If value < tester:
        if (list(j) < tester) then

            ! add it to the new list
            new_list_count = new_list_count + 1
            temp_list(new_list_count) = list(j)

        ! If value = tester:
        else if (list(j) == tester) then

            ! do not add to the new list
            ! iterate to the next value of the tester
            tester = tester + base_prime

        ! If value > tester:
        else

            ! add it to the new list
            new_list_count = new_list_count + 1
            temp_list(new_list_count) = list(j)

            ! iterate to the next value of the tester
            tester = tester + base_prime

        end if

    end do

    ! Reshape array
    deallocate(list)
    allocate(list(new_list_count))

    do j = 1, new_list_count
        list(j) = temp_list(j)
    end do

end do


! Print out final list of prime numbers
write(*,*) "Prime numbers up to ", n
do j = 1, size(list)
    write(*,*) list(j)
end do


end program
