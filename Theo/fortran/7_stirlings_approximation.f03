program stirlings_approximation

use maths

implicit none
integer, dimension(4) :: N
integer :: i
real(dp) :: exact, stirling, error

N(1) = 10
N(2) = 100
N(3) = 1000
N(4) = 5000

do i = 1, size(N)

    call exact_solution(N(i), exact)
    call stirling_approximation(N(i), stirling)

    error = stirling - exact
    error = (100.d0 * error) / exact

    write(*,*) "N =", N(i)
    write(*,*) "Exact Solution =", exact
    write(*,*) "Stirling's Approximation =", stirling
    write(*,*) "Percent Error =", error
    write(*,*) ""

end do


contains

! Calculate the exact solution of log(N!)
subroutine exact_solution(N, solution)

    implicit none
    integer, intent(in) :: N
    real(dp), intent(out) :: solution
    integer :: i

    solution = 0.d0

    do i = 1, N
        solution = solution + log(i*1.d0)
    end do

end subroutine

! Calculate Stirling's Approximation
subroutine stirling_approximation(N, solution)

    implicit none
    integer, intent(in) :: N
    real(dp), intent(out) :: solution

    solution = log(N*1.d0) - 1.d0
    solution = solution * N * 1.d0

end subroutine

end program
