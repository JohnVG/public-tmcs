STILl TO BE FINISHED!

Projects 1,2,3,4 (all work in python)
Project 16 (work in progress)


------------------

HOW TO COMPILE

To compile a program type the following into a terminal:
> make n
where n is the number of the project. The program will be called "programn.exe"
For example:
> make 7
will make "program7.exe".

This is equivalent to the following terminal input:
> gfortran -O3 maths.f03 <filename.f03> -o <program_name.exe>

Type the following:
> make clean
To delete all files other than input files or source files.

------------------

MODULES

maths.f03 is a module I wrote to allow for double precision real numbers. (dp)

------------------

INPUT

I use namelists for input. Hopefully, it should be quite easy to follow how this works, but if not, ask me.
