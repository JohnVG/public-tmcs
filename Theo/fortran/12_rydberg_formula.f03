program rydberg_formula

use maths
implicit none

real(dp), dimension(:), allocatable :: frequencies
real(dp) :: A, frac_tolerance, tolerance, ratio, diff

integer, dimension(:), allocatable :: true_n1, true_n2
integer :: i, n_freq, n1, n2

logical :: answer_found, n1_equal, n2_equal

! Rydberg Constant
real(dp), parameter :: RH = 109737.32d0


! Read in variables from input file
namelist /rydberg1/ n_freq, frac_tolerance
namelist /rydberg2/ frequencies

open(101, file='input.in')
read(101, nml=rydberg1)

allocate(frequencies(n_freq))
read(101, nml=rydberg2)

close(101)


! Allocate output arrays
allocate(true_n1(n_freq))
allocate(true_n2(n_freq))


! NOTE: set to read in
frac_tolerance = 0.01d0                ! Fractional tolerance for correct answer



! Sweep through frequency values
do i = 1, n_freq

    ! Calculate target fraction
    A = frequencies(i) / RH

    ! Calculate absolute tolerance for A frequency
    tolerance = A * frac_tolerance

    ! Initiate
    answer_found = .false.


    ! Sweep through n1
    n1 = 1
    do

        ! Sweep through n2
        n2 = n1 + 1
        do

            ! Calculate ratio of frequencies from n1 and n2 and Rydberg formula
            call rydberg(n1,n2,ratio)

            ! Calculate the difference
            diff = ratio - A

            ! Check if difference is within tolerance
            ! If it is, set logical and exit
            if (abs(diff) < tolerance) then

                true_n1(i) = n1
                true_n2(i) = n2

                answer_found = .true.

                exit

            ! Exit the inner loop if difference becomes postive
            ! This means n1 needs to be iterated to the next value
            else if (diff > 0.d0) then
                exit

            ! Iterate to next n2
            else
                n2 = n2 + 1
            end if

        end do

        ! Exit the loop if answer is found
        if (answer_found) exit

        ! Iterate to next n1
        n1 = n1 + 1

    end do

end do


! Check if all n1 and n2 are identical
call equal_check(true_n1, n1_equal)
call equal_check(true_n2, n2_equal)

if (n1_equal) then
    write(*,*) "All lines originate at n1 =", true_n1(1)

else if (n2_equal) then
    write(*,*) "All lines terminate at n2 =", true_n2(1)

else
    write(*,*) "Lines do not form a set"

end if


! Print out solutions
write(*,*) "  Frequencies                       n1          n2"

do i = 1, n_freq

    write(*,*) frequencies(i), true_n1(i), true_n2(i)

end do


contains

! Calculates ratio of frequency to Rydberg constant
subroutine rydberg(n1, n2, ratio)

    implicit none

    integer, intent(in) :: n1, n2
    real(dp), intent(out) :: ratio

    ratio = (1.d0/(1.d0*n1*n1)) - (1.d0/(1.d0*n2*n2))

end subroutine


! Checks if all items in the list are identical
subroutine equal_check(list, equal)

    implicit none
    integer, dimension(:), intent(in) :: list
    logical, intent(out) :: equal

    integer :: test, i

    ! Initiate
    ! Assume all items in list are equal unless proven otherwise
    test = list(1)
    equal = .true.

    do i = 2, size(list)

        ! Set logical to false and exit if unequal item is found in list
        if (list(i) > test) then

            equal = .false.
            exit

        else if (list(i) < test) then

            equal = .false.
            exit

        end if

    end do

end subroutine


end program
