program quadratic_equation

use maths

implicit none
integer :: a, b, c
real(dp) :: disc, A1, A2

! Read in variables
write(*,*) "Input a:"
read(*,*) a

write(*,*) "Input b:"
read(*,*) b

write(*,*) "Input c:"
read(*,*) c

! Calculate discriminant and abort if negative
call calculate_discriminant(a,b,c,disc)

! Compute the rest of the equation
A1 = -(1.d0*b) + disc
A2 = -(1.d0*b) - disc

A1 = A1 / (2.d0*a)
A2 = A2 / (2.d0*a)

write(*,*) "Solutions"
write(*,*) A1, A2


contains

subroutine calculate_discriminant(a,b,c,disc)

    implicit none
    integer, intent(in) :: a, b, c
    real(dp), intent(out) :: disc

    ! Calculate Discriminant
    disc = (1.d0*b*b) - (4.d0*a*c)

    ! Abort if quadratic has no real roots
    if (disc < 0.d0) then
        write(*,*) "Discriminant less than 0 - Quadratic does not have any real roots"
        stop

    else
        disc = sqrt(disc)

    end if

end subroutine

end program
