program hydrogenation_of_ethylene

use maths

implicit none
real(dp) :: k, H2, c, c_min, c_max, dc, C2H4, C2H4_min, C2H4_max, dC2H4, kH2
integer :: n_c, n_C2H4, i, j
real(dp), dimension(:), allocatable :: rate


! Read in variables from input file
namelist /hydrogenation/ k, H2, c_min, c_max, n_c, C2H4_min, C2H4_max, n_C2H4

open(101, file='input.in')
read(101, nml=hydrogenation)
close(101)

! Open output file
open(102, file="11_hydrogenation_of_ethylene.out")

! Allocate output array
allocate( rate(n_c) )

! Calculate variables
kH2 = k * H2

! Calculate difference between each value of c and C2H4
n_c = n_c - 1
n_C2H4 = n_C2H4 -1

dc = (c_max - c_min) / (1.d0 * n_c)
dC2H4 = (C2H4_max - C2H4_min) / (1.d0 * n_C2H4)


! Produce output legend
do j = 0, n_c
    rate(j+1) = c_min + (j*dc)
end do

write(102,*) "  [C2H4]              c =", rate


! Sweep through values of C2H4
do i = 0, n_C2H4

    C2H4 = C2H4_min + (i*dC2H4)

    ! Sweep through values of c
    do j = 0, n_c

        c = c_min + (j*dc)

        ! Calculate and output rate at C2H4 for all values of c
        call calculate_rate(kH2, c, C2H4, rate(j+1))

    end do

    write(102,*) C2H4, rate

end do

close(102)


contains

subroutine calculate_rate(kH2, c, C2H4, rate)

    implicit none
    real(dp), intent(in) :: kH2, c, C2H4
    real(dp), intent(out) :: rate

    ! NOTE: kH2 = k * H2

    rate = 1.d0 + (c*C2H4)
    rate = rate * rate
    rate = (kH2 * C2H4) / rate

end subroutine

end program
