program bubble_sort

implicit none
integer, dimension(100) :: list
integer :: j

! Read in list of random numbers
open(101,file="random_numbers.txt")

do j = 1, 100
    read(101,*) list(j)
end do

! Sort
call bubble_sort_algorithm(list)

! Output the sorted list
do j = 1, 100
    write(*,*) list(j)
end do



contains

subroutine bubble_sort_algorithm(list)

    implicit none
    integer, dimension(:), intent(inout) :: list
    integer :: n, i, j, x, y
    logical :: sorted

    ! Read length of list
    n = size(list)


    ! Loop should escape before j = n
    do j = 1, n

        ! Assume list is sorted
        sorted = .true.

        ! Sweep through pairs of the lsit
        do i = 1, n-1

            x = list(i)
            y = list(i+1)

            ! If pair is found out of order:
            ! Correct order and set state of list as still out of order to ensure another pass through
            if (x>y) then

                list(i) = y
                list(i+1) = x

                sorted = .false.

            end if

        end do

        ! If the list is passed through with no corrections, the list is in order
        ! Therefore, exit
        if (sorted) exit

    end do

end subroutine

end program
