program insertion_sort

implicit none
integer, dimension(100) :: list
integer :: j

! Read in list of random numbers
open(101,file="random_numbers.txt")

do j = 1, 100
    read(101,*) list(j)
end do

! Sort
call insertion_sort_algorithm(list)

! Output the sorted list
do j = 1, 100
    write(*,*) list(j)
end do


contains

subroutine insertion_sort_algorithm(list)

    implicit none
    integer, dimension(:), intent(inout) :: list
    integer, dimension(:), allocatable :: temp_list
    integer :: n, x, j, i, test, k


    ! Allocate the temporary list
    n = size(list)
    allocate(temp_list(n))

    temp_list(1) = list(1)


    ! Go through remaining values in the list
    do j = 2, n

        x = list(j)

        ! Check if value to be inserted is the new maximum value
        test = temp_list(j-1)

        ! If it is, then insert the new maximum at the end of the output list
        if (x > test) then
            temp_list(j) = x

        ! Otherwise, go through values in output list
        else

            do i = 1, j-1

                test = temp_list(i)

                ! If a value is reached that is greater than the value to be inserted:
                if (x <= test) then

                    ! Shift all the values above the place to be inserted
                    do k = j, i+1, -1
                        temp_list(k) = temp_list(k-1)
                    end do

                    ! Insert the new value at the ith position
                    temp_list(i) = x

                    exit

                end if

            end do

        end if

    end do

    ! Set the output list as the sorted temporary list
    list = temp_list
    deallocate(temp_list)

end subroutine

end program
