program numerical_integration

use maths
implicit none

real(dp) :: a, b, I, exact
integer :: n

! Start and end points of integral
a = 0.0
b = 1.0

! Number of blocks to use in integration
n = 10


! Analytical solution
! 3*x*x + 2*x     ->      x**3 + x**2s
! Between 1 and 0
! I = 2
exact = 2.d0

write(*,*) "Analytical Result =", exact

call midpoint(a,b,n,I)
write(*,*) "Midpoint Rule =", I

call trapezium(a,b,n,I)
write(*,*) "Trapezium Rule =", I

call simpson(a,b,n,I)
write(*,*) "Simpson's Rule =", I



contains

real(dp) function f(x)

    implicit none
    real(dp), intent(in) :: x

    f = (3.d0*x*x) + (2.d0*x)

end function


! Integration by midpoint rule
! n = number of blocks
subroutine midpoint(a,b,n,I)

    implicit none
    real(dp), intent(in) :: a,b
    integer, intent(in) :: n
    real(dp), intent(out) :: I

    integer :: j
    real(dp) :: block_width, x

    ! Calculate the width of each rectangle
    block_width = (b-a) / (1.d0*n)


    ! Initial
    I = 0.d0

    ! Calculate integral
    do j = 1, n

        ! Calculate midpoint of each block
        x = a + (j*block_width) - (0.5d0 * block_width)

        ! Contribute to integral
        I = I + f(x)

    end do

    ! Multiply by correct factor
    I = I * block_width

end subroutine


! Integration by midpoint rule
! n = number of trapeziums
subroutine trapezium(a,b,n,I)

    implicit none
    real(dp), intent(in) :: a, b
    integer, intent(in) :: n
    real(dp), intent(out) :: I

    real(dp) :: trapezium_width, x
    integer :: j

    ! Calculate trapezium width
    trapezium_width = (b-a) / (n*1.d0)

    ! Initial
    I = 0.d0

    do j = 0, n

        ! Calculate value of x
        x = a + (j*trapezium_width)

        ! Contribute to the integral
        I = I + f(x)

    end do

    ! Account for double counting of the first and last values of x
    I = I - (0.5d0 * f(a))
    I = I - (0.5d0 * f(b))

    ! Multiply by the correct factor
    I = I * trapezium_width

end subroutine


! Integration by Simpson's rule
! n = number of blocks
subroutine simpson(a,b,n,I)

    implicit none
    real(dp), intent(in) :: a, b
    integer, intent(in) :: n
    real(dp), intent(out) :: I

    real(dp) :: width, x
    integer :: j
    logical :: even

    ! Calculate trapezium width
    width = (b-a) / (n*1.d0)

    ! Initial
    I = 0.d0

    ! Calculate sum of f(x)
    ! Set first number added as even
    even = .true.

    do j = 0, n

        x = a + (width * j)

        ! Contribution of even terms
        if (even) then
            I = I + f(x)
            even = .false.

        ! Contribution of odd terms
        else
            I = I + (2.d0 * f(x))
            even = .true.

        end if

    end do

    ! Multiply so there are 2x of all even terms and 4x of all odd terms
    I = I * 2.d0

    ! Account for double counting of the first and last value
    I = I - f(a)
    I = I - f(b)

    ! Multiply by correct factor
    I = (I * width) / 3.d0

end subroutine

end program
