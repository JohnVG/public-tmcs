program van_der_waals_equation

use maths

implicit none
real(dp) :: R, a, b, n, T, V1, V2, nRT, nb, nna, V, dV, p
integer :: i, number

! Read in variables from input file
namelist /VDW/ a, b, n, T, V1, V2

open(101, file='input.in')
read(101, nml=VDW)
close(101)

R = 8.314d0
number = 9

! Calculate constants used
call VDW_constants(n, T, a, b, nRT, nb, nna)

! Calculate difference between successive values of V
dV = (V2 - V1) / (number * 1.d0)

! Initial
write(*,*) "  Volume                    Pressure"

do i = 0, number

    ! Calculate V values
    V = V1 + (dV * i)

    ! Calculate pressure
    call VDW_pressure(V, nRT, nb, nna, p)

    ! Output
    write(*,*) V, p

end do


contains

! Calculate constants used in calculating pressure for a range of volumes
subroutine VDW_constants(n, T, a, b, nRT, nb, nna)

    implicit none
    real(dp), intent(in) :: n, T, a, b
    real(dp), intent(out) :: nRT, nb, nna

    nRT = n*R*T
    nb = n*b
    nna = n*n*a

end subroutine


! Calculate the pressure of a Van der Waals gas
subroutine VDW_pressure(V, nRT, nb, nna, p)

    implicit none
    real(dp), intent(in) :: V, nRT, nb, nna
    real(dp), intent(out) :: p

    p = (nRT)/(V - nb)
    p = p - ( nna/(V*V) )

end subroutine

end program
