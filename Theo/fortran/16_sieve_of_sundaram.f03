program sieve_of_sundaram

implicit none
integer :: i, j, n, x
integer, dimension(:), allocatable :: list
logical :: finished


! Generate list of numbers
n = 50
allocate(list(n))

do j = 1,n
    list(j) = j
end do


! Sweep through all values of i ≥ 1
do i = 1, n

    ! Assume the inner loop will exit in the first pass
    finished = .true.

    ! Sweep through all values of j ≥ i
    do j = i, n

        ! Calculate the test value
        x = tester(i,j)

        ! If test value exceeds n, exit
        if (x>n) exit

        ! Try and remove the tester from the list
        call remove(list, x)

        ! If the first pass completes, set the logical to false
        ! There may be more primes with larger values of i
        finished = .false.

    end do

    ! If the tester was > n for the first value of j
    ! There cannot be any more primes
    ! Exit the program here
    if (finished) exit

end do


! Double the list and add 1 to produce the final result
list = 2*list
list = list + 1

! Print out final list of prime numbers
write(*,*) "Prime numbers up to ", 2*(n+1)
do j = 1, size(list)
    write(*,*) list(j)
end do


contains

function tester(i,j)

    implicit none
    integer, intent(in) :: i, j
    integer :: tester

    tester = i + j + (2*i*j)

end function

! Remove a value from a list arranged in increasing order
subroutine remove(list, value)

    implicit none
    integer, dimension(:), allocatable, intent(inout) :: list
    integer, intent(in) :: value

    integer, dimension(:), allocatable :: temp_list
    integer :: n, j, j_to_remove, k
    logical :: removing_a_value

    n = size(list)

    ! Sweep through the values of the list
    do j = 1, n

        ! If an exact match is found
        if (list(j) == value) then

            ! Escape, and note which value to not add
            removing_a_value = .true.
            j_to_remove = j
            exit


        ! If we exceed the value without finding an exact match
        else if (list(j) > value) then

            ! The value isn't in the list, and we just output the input list again
            removing_a_value = .false.
            exit

        end if

    end do


    ! If we have to remove a value
    if (removing_a_value) then

        ! Allocate the temporary array to be one smaller than the input array
        allocate(temp_list(n-1))

        ! Add all values up to the value to be removed
        do j = 1, j_to_remove-1
            temp_list(j) = list(j)
        end do

        ! Skip over the value to be removed
        do j = j_to_remove+1, n
            temp_list(j-1) = list(j)
        end do

        ! Resize and copy the original list
        deallocate(list)
        allocate(list(n-1))
        list = temp_list

    end if

end subroutine

end program
