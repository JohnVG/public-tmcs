program infrared_spectrum_of_HCl

use maths
implicit none

real(dp) :: B, T, double_B, constant, freq
real(dp), dimension(:), allocatable :: R_intensity, P_intensity, R_freq, P_freq
integer :: i, j, n_peaks


! Read in variables from input file
namelist /infrared/ B, T, n_peaks

open(101, file='input.in')
read(101, nml=infrared)
close(101)


! Calculate constants
constant = B / (0.69503d0 * T)
double_B = 2.d0 * B


! Calculate intensity of each peak
allocate( R_intensity(n_peaks) )
allocate( P_intensity(n_peaks) )

do i = 1, n_peaks

    ! j starts at 0
    j = i - 1
    freq = (2.d0*j) + 1.d0
    R_intensity(i) = freq * exp( -constant * j * (j+1) )

end do

! As P branch starts at J=1, set P branch intensity at j=0 to 0
P_intensity = R_intensity
P_intensity(1) = 0.d0


! Calculate R and P branch frequencies
allocate( R_freq(n_peaks) )
allocate( P_freq(n_peaks) )

P_freq(1) = 0.d0    ! First value is filler to represent the vacant j=0 peak

do j = 1, n_peaks-1

    freq = double_B * j

    ! Actually calculating the (j-1)th value of the R-branch
    R_freq(j) = freq

    ! Calculating the jth value of the P-branch
    P_freq(j+1) = -freq

end do

! Calculate the final R-branch frequency to ensure both brances have equal number of peaks
freq = double_B * n_peaks
R_freq(n_peaks) = freq


! Output
open(102, file="infrared.out")

do i = 1, n_peaks
    write(102,*) R_freq(i), R_intensity(i), P_freq(i), P_intensity(i)
end do

close(102)


end program
