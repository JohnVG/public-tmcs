import numpy as np

filename = "random_numbers.txt"
list = np.loadtxt(filename, dtype="int")
list = list.tolist()

# Generate an output array and populate it with a single value
x = list.pop()
output = [x]

# Go through remaining values in the list
for i in list:

    # Pop a value to be sorted from the input array
    x = list.pop()

    # Check if value to be inserted is the new maximum value
    test = output[-1]

    # If it is, then insert the new maximum at the end of the output list
    if (x > test):
        output.append(x)

    # Otherwise, go through values in output list
    else:
        for j in range(0, len(output)):

            # If a value is reached that is greater than the value to be inserted:
            # Insert the value and exit
            if (x <= output[j]):
                output.insert(j,x)
                break

# Print sorted array
print(output)
