import numpy as np

filename = "random_numbers.txt"
list = np.loadtxt(filename, dtype="int")
list = list.tolist()

print(list)


def split(list):

    # Initial
    less = []
    more = []

    # Pop a pivot value from the list
    pivot = list.pop()

    # Sweep through the remaining values in the list
    # Sort list according to the pivot
    for x in list:

        if (x < pivot):
            less.append(x)

        else:
            more.append(x)

    # Determine where the pivot is in the list
    pivot_position = len(less)

    # Create one output list
    less.append(pivot)
    less.extend(more)

    return less, pivot_position


# Initiate range of list to extract working list from
pivots = [-1, len(list)]

# Initialise to enter the sorting loop
sorted = False

while (not sorted):

    # Select range of values to extract from list
    a = pivots[0] + 1
    b = pivots[1]

    # Extract the working list from the main list
    work_list = []

    for i in range(a,b):
        work_list.append( list[i] )

    # Split the worklist according to pivot
    work_list, new_pivot = split(work_list)

    # Reinsert work list
    for i in range(0, len(work_list)):
        list[a+i] = work_list[i]


    # Locate where the new pivot point is in the total list
    new_pivot = a + new_pivot
    pivots.insert(1, new_pivot)


    # Check if next work list has reached minimum size
    # If the work list to be extracted is only 1 element, it must already be sorted
    # Loop is needed here, as the second working list could also be minimum size
    looping = True

    while looping:

        # Calculate size of the working list
        difference = pivots[1] - pivots[0]

        # Clear the working list from the pivots list if below minimum size
        if (difference <= 2):
            pivots.pop(0)

        else:
            looping = False

        # Escape condition once the end of the list has been reached
        if (len(pivots) < 2):
            sorted = True
            break


print(list)
