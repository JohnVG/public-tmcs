n = 25

# Initialise Fibonacci Series
series = [1,1]

# Calculate terms up to the nth term
for i in range(2,n):

    n = len(series)
    next_term = series[n-1] + series[n-2]
    series.append(next_term)

print(series)
