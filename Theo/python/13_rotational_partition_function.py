import numpy as np
import matplotlib.pyplot as plt


# Calculates the rotational partition function
def rotational_partition_function(Bbeta, cutoff):

    # j = 0 term
    j = 0
    term = 1.0
    f = term

    # Remaining terms
    while (term > (f*cutoff)):

        j = j + 1

        term = (2*j) + 1
        term = term * np.exp( -Bbeta * j * (j+1) )

        f = f + term

    return f


# Define variables
# Fractional cutoff at which point to neglect further terms
cutoff = 0.001

# Boltzmann Constant
k = 1.38066E-23               # in JK-1
k = k / 1.986E-23   # in cm-1 K-1

# Temperature
T = np.arange(start=5.0, stop=165.0, step=10.0)   # in K

# Rotational Constants
B_list = {"HCl":15.2, "H2":85.4, "NO":2.9}
colors = {"HCl":"red", "H2":"green", "NO":"blue"}


for species in B_list:

    B = B_list[species]

    # Calculate constant used in calculation of partition function
    Bbeta = B / (k * T)

    # Calculate values of rotational partition function
    rot_pf = []

    for values in Bbeta:

        f = rotational_partition_function(values, cutoff)
        rot_pf.append(f)

    # Plot results
    plt.plot(T, rot_pf, label=species+" Exact", color=colors[species], linewidth=1.5)
    plt.plot(T, T/B, linestyle=":", label=species+" Approx", color=colors[species], linewidth=1.5)


# Matplotlib settings
plt.xlabel("Temperature / K")
plt.ylabel("Rotational Partition Function")
plt.legend(loc="upper left")
plt.show()
