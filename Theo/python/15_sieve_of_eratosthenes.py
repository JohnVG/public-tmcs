# Generate a list of numbers up to n
n = 500
list = []

for j in range(2,n+1):

    list.append(j)


# Initiate
base_prime = 0
i = 0

# Exit Condition
while ( n > (base_prime*base_prime) ):

    # Read in the prime from the list
    base_prime = list[i]
    tester = base_prime

    # Condition for when the whole list has been checked
    while (tester < n):

        # Advance test number
        tester += base_prime

        # Remove the number if it exists in the list
        try:
            list.remove(tester)

        except:
            pass

    # Advance index to check against next prime in the list
    i += 1


# Print out final list of prime numbers
print("Prime numbers up to %d" % n)
print(list)
