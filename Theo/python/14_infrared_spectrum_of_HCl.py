import numpy as np
import matplotlib.pyplot as plt


# Define variables and calculate constants
B = 10.0
T = 300.0

constant = B / (0.69503 * T)
double_B = 2.0 * B

# Number of peaks to produce
n_peaks = 15


# Calculate intensity of each peak
R_intensity = []

for j in range(0, n_peaks):

    I = (2*j) + 1
    I = I * np.exp( -constant * j * (j+1) )

    R_intensity.append(I)

# As P branch starts at J=1, set P branch intensity at j=0 to 0
P_intensity = R_intensity
P_intensity[0] = 0.0


# Calculate R and P branch frequencies
freq_R = []
freq_P = [0]    # First value is filler to represent the vacant j=0 peak

for j in range(1,n_peaks):

    freq = double_B * j

    # Actually calculating the (j-1)th value of the R-branch
    freq_R.append(freq)

    # Calculating the jth value of the P-branch
    freq_P.append(-freq)

# Calculate the final R-branch frequency to ensure both brances have equal number of peaks
freq = double_B * n_peaks
freq_R.append(freq)


# Plot out R and P branches
plt.bar(freq_R, R_intensity)
plt.bar(freq_P, P_intensity)


# Matplotlib Settinds
plt.xlabel("Frequency / cm-1")
plt.ylabel("Intensity")
plt.legend()
plt.show()
