import math
import sys

a = input("Input a:")
b = input("Input b:")
c = input("Input c:")

# Calculate Discriminant
disc = (b*b) - (4*a*c)

# Abort if quadratic has no real roots
if (disc < 0):
    sys.exit("Discriminant less than 0 - Quadratic does not have any real roots")

disc = math.sqrt(disc)

# Compute the rest of the equation
A1 = -b + disc
A2 = -b - disc

A1 = A1 / (2*a)
A2 = A2 / (2*a)

print("Solutions")
print(A1, A2)
