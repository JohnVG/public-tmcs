import numpy as np


# Bubble Sort Algorithm
def bubble_sort(list):

    # Read length of list
    n = len(list)

    # Set default state of list as out of order
    out_of_order = True

    while out_of_order:

        # Assume list is sorted
        out_of_order = False

        # Sweep through pairs of the lsit
        for i in range(0,n-1):

            x = list[i]
            y = list[i+1]

            # If pair is found out of order:
            # Correct order and set state of list as still out of order to ensure another pass through
            if (x>y):

                list[i] = y
                list[i+1] = x

                out_of_order = True

    return list


# Main body of code

filename = "random_numbers.txt"
list = np.loadtxt(filename, dtype="int")
list = list.tolist()

print(list)
list = bubble_sort(list)
print(list)
