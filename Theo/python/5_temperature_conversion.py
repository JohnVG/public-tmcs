import numpy as np

# Constants
a = 9.0 * 0.2
b = 32.0

def farenheit(celsius):

    F = (a * celsius) + b
    return F

# Convert individual temperature

C = input("Input temperature in celsius:")
F = farenheit(C)

print("Temperature in farenheit:", F)
print("")


# Produce a conversion table
C = np.arange(0.0, 101.0, 1.0)
F = farenheit(C)

print(["Celsius", "Farenheit"])

for i in range(0, 101):

    print([C[i], F[i]])
