def rydberg(n1,n2):

    n1_n1 = n1*n1
    n2_n2 = n2*n2

    return (1.0/n1_n1) - (1.0/n2_n2)

# Ryberg Constant
RH = 109737.32

# Input frequency
frequencies = [9953.50, 5503.80, 1341.23, 359.87]

true_n1 = []
true_n2 = []

for freq in frequencies:

    # Calculate target fraction
    target = freq / RH

    # Tolerance for correct answer
    frac_tolerance = 0.01                   # Fractional tolerance for correct answer
    tolerance = target * frac_tolerance     # Absolute tolerance for correct answer

    # Initiate
    n1 = 1
    answer_found = False

    # Loop to sweep through n1 values
    while (not answer_found):

        # Set minimum value of n2
        n2 = n1 + 1

        # Initiate difference to get into the nested loop
        diff = -1.0

        # Exit loop and iterate to next n1 if answer begins to exceed target
        # Loop to sweep through n2 values
        while (diff < 0):

            # Calculate ratio of frequencies from n1 and n2 and Rydberg formula
            A = rydberg(n1,n2)

            # Calculate the difference
            diff = A - target

            # Check if difference is within tolerance
            if (abs(diff) < tolerance):

                true_n1.append(n1)
                true_n2.append(n2)
                answer_found = True

            n2 += 1

        # Iterate to next n1
        n1 += 1


# Checks if all items in the list are identical
def equal_check(list):

    test = list[0]
    logical = True

    for item in list:

        if (item == test):
            pass

        else:
            logical = False
            break

    return logical

# Check if n1 and n2 are identical
if (equal_check(true_n1)):
    print("All lines originate at n1 = %d" % true_n1[0])

elif (equal_check(true_n2)):
    print("All lines originate at n2 = %d" % true_n2[0])

else:
    print("Lines do not form a set")


# Print out solutions
print("Frequencies", "n1", "n2")

for i in range(0,len(frequencies)):
    print(frequencies[i], true_n1[i], true_n2[i])
