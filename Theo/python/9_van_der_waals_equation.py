import numpy as np

# Calculates the pressure of a Van der Waals gas
def VDW_constants(n,T,a,b):

    nRT = n*R*T
    nb = n*b
    nna = n*n*a

    return nRT, nb, nna


def van_der_vaals_pressure(V,nRT,nb,nna):

    p = (nRT)/(V - nb)
    p = p - ( nna/(V*V) )

    return p


# Define constants and input variables
R = 8.314

a = input("Input a:")
b = input("Input b:")
n = input("Input n:")
T = input("Input T:")
V1 = input("Input V1:")
V2 = input("Input V2:")

# Calculate constants used
nRT, nb, nna = VDW_constants(n,T,a,b)

# Calculate range of V and p values
V = np.linspace(start=V1, stop=V2, num=10)

p = van_der_vaals_pressure(V,nRT,nb,nna)

# Output
print("Volume, Pressure")
for i in range(0,10):
    print(V[i],p[i])
