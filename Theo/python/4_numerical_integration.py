import numpy as np

# Function
def f(x):
    return (3.0*x*x) + (2.0*x)


# Integration by midpoint rule
def midpoint(a,b,n):

    # Calculate the width of each rectangle
    block_width = (b-a) / n

    # Generate mid point of each block
    block = np.linspace(a, b-block_width, n)
    block = block + (0.5*block_width)

    # Initial
    I = 0

    # Calculate integral
    for x in block:
        I += f(x)

    return I * block_width


# Integration by trapezium rule
# n = number of points
def trapezium(a,b,n):

    # Calculate trapezium width
    trapezium_width = (b-a) / (n-1)

    # Calculate points
    block = np.linspace(a, b, n)

    # Initial
    I = 0

    # Calculate sum of f(x)
    for x in block:
        I += f(x)

    I = I - ( 0.5 * f(block[0]) )
    I = I - ( 0.5 * f(block[-1]) )

    return I * trapezium_width


# Integration by Simpson's rule
# n = number of points
def simpson(a,b,n):

    # Calculate trapezium width
    width = (b-a) / (n-1)

    # Calculate points
    block = np.linspace(a, b, n)

    # Initial
    I = 0

    # Calculate sum of f(x)
    even = True  # Set first number added as even

    for x in block:

        # Contribution of even terms
        if even:
            I = I + f(x)
            even = False

        # Contribution of odd terms
        else:
            I = I + (2.0 * f(x))
            even = True

    I = I * 2.0
    I = I - f(block[0])
    I = I - f(block[-1])

    return (I * width) / 3.0


# Start and end points of integral
a = 0.0
b = 1.0

# Number of points
n = 11

I = midpoint(a,b,n-1)
print("Midpoint Rule = %s" % I)

I = trapezium(a,b,n)
print("Trapezium Rule = %s" % I)

I = simpson(a,b,n)
print("Simpson's Rule = %s" % I)
