import numpy as np
import matplotlib.pyplot as plt


# Equation to calculate rate
def calculate_rate(kH2,c,C2H4):

    # NOTE: kH2 = k * H2

    rate = 1.0 + (c*C2H4)
    rate = rate * rate
    rate = (kH2 * C2H4) / rate

    return rate


# Define variables
k = 4.0E5
H2 = 1.0
c = np.linspace(start=0.0, stop=20.0, num=5)
C2H4 = np.linspace(start=0.0, stop=0.05, num=101)

kH2 = k * H2

# Sweep over values of c
for c_values in c:

    # Calculate rate at each value of c
    rate = calculate_rate(kH2, c_values, C2H4)

    # Plot the resulting rate
    label_string = "c = " + str(c_values)
    plt.plot(C2H4, rate, label=label_string, linewidth=1.5)


# Pyplot settings
plt.xlabel("[C2H4]")
plt.ylabel("Rate")
plt.legend()
plt.show()
