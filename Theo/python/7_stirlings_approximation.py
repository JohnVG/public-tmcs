import math


# Calculate the exact solution of log(N!)
def exact_solution(N):

    output = math.log(N)

    for i in range(1,N):
        output = output + math.log(i)

    return output


# Calculate Stirling's Approximation
def stirling_approximation(N):

    output = math.log(N) - 1.0
    output = output * N

    return output


# Main Body
for N in [10, 100, 1000, 5000]:

    exact = exact_solution(N)
    stirling = stirling_approximation(N)

    error = stirling - exact
    error = (100*error) / exact

    print("N = %s" % N)
    print("Exact Solution = %s" % exact)
    print("Stirling's Approximation = %s" % stirling)
    print("Percent Error = %s" % error)
    print("")
