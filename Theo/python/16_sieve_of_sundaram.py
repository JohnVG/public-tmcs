def tester(i,j):
    return i + j + (2*i*j)


# Generate list of numbers
n = 500
list = []

for j in range(1,n+1):
    list.append(j)


# Initiate
i = 1
logical = True

while logical:

    # Assume that the loop will termintate while i=j
    logical = False

    for j in range(i,n):

        # Calculate value
        x = tester(i,j)

        # Escape if value exceeds n
        if (n<x):
            break

        else:

            # Remove value from list
            try:
                list.remove(x)

            except:
                pass

            # Otherwise, adjust logical as the loop will not terminate on i=j
            logical = True

    # Advance to next value
    i += 1

# Convert initial list to final list of odd primes
for i in range( len(list) ):

    list[i] = (2*list[i]) + 1


# Output final list of odd primes below 2n+2
print(list)
