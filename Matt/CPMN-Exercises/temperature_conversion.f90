MODULE temperature_conversion
    ! Contains answers for Q5 of short_projects.pdf
    use, intrinsic :: iso_fortran_env
    IMPLICIT NONE
    integer, parameter :: dp = REAL64
    PRIVATE
    PUBLIC :: fahrenheit_to_celsius, celsius_to_fahrenheit
contains
    pure real(kind=dp) function fahrenheit_to_celsius(temp)
        IMPLICIT NONE
        real(kind=dp), intent(in) :: temp
        
        fahrenheit_to_celsius = (temp - 32.0_dp) / 1.8_dp
    end function fahrenheit_to_celsius
    
    pure real(kind=dp) function celsius_to_fahrenheit(temp)
        real(kind=dp), intent(in) :: temp
        
        celsius_to_fahrenheit = (temp - 32.0_dp) / 1.8_dp
    end function celsius_to_fahrenheit
end module