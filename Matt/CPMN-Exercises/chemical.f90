module chemical
    ! Contains answers for Q6, Q7 and Q8 of short_projects.pdf
    use, intrinsic :: iso_fortran_env
    use mathematical, only: approx_equal
    IMPLICIT NONE
    integer, parameter :: dp = REAL64
    PRIVATE
    PUBLIC :: van_der_waals, mass_spec_unknowns, hydrogenation_rate
contains
    pure real(kind=dp) function van_der_waals(V, a, b, T, n)
        IMPLICIT NONE
        ! Volume is in m^3, a and b are in Pa m^6 mol^-2, and T is in K.
        real(kind=dp), intent(in) :: V, a, b, T, n
        real(kind=dp), parameter :: gas_constant = 8.314 ! JKmol^-1
        van_der_waals = ((n * gas_constant * T) / (V - (n *b))) - ((n**2 * a**2) / V**2)
        return
    end function van_der_waals
    
    pure function mass_spec_unknowns(inmass)
        ! This is a horrible method to solve a 
        ! horrible question. Uses the "just try everything" method.
        IMPLICIT NONE
        integer, dimension(4) :: mass_spec_unknowns
        integer :: a, b, c, d
        real(kind=dp), intent(in) :: inmass
        real(kind=dp) :: workingmass
        real(kind=dp), dimension(4), parameter :: masses = [12.0038_dp, 14.0075_dp, 16.0000_dp, 1.0081_dp]

        do a=0, min(10, int(inmass/masses(1)))
            workingmass = inmass - (a * masses(1))
            do b=0, int(workingmass / masses(2))
                workingmass = inmass - (a * masses(1)) - (b * masses(2))
                do c=0, int(workingmass / masses(3))
                    workingmass = inmass - (a * masses(1)) - (b * masses(2)) - (c * masses(3))                    
                    do d=0, int(workingmass / masses(4))
                        workingmass = inmass - (a * masses(1)) - (b * masses(2)) - (c * masses(3)) - (d * masses(4))
                        if (approx_equal(0.0_dp, workingmass))then
                            mass_spec_unknowns = [a, b, c, d]
                            return
                        end if
                    end do
                end do
            end do
        end do
    mass_spec_unknowns = [-1, -1, -1, -1]
    return
    end function mass_spec_unknowns
    
    pure real(kind=dp) function hydrogenation_rate(conc_H2, conc_ethene, c) 
        IMPLICIT NONE
        real(kind=dp), intent(in) :: conc_H2, conc_ethene, c
        real(kind=dp), parameter :: k = 4.0e5_dp
        hydrogenation_rate = (k * conc_H2 * conc_ethene) / ((1 + (c * conc_ethene))**2)
    end function hydrogenation_rate
        
end module chemical
