MODULE mathematical
    ! Contains answers for Q6, Q7 and Q8 of short_projects.pdf
    use, intrinsic :: iso_fortran_env
    IMPLICIT NONE
    integer, parameter :: dp = REAL64
    PRIVATE
    PUBLIC :: quadratic_equation, stirlings_approx, log_n_factorial, approx_equal, fibonacci
contains

    pure function fibonacci(n) result(fib_arr)
        implicit none
        integer, intent(in) :: n
        integer, dimension(n) :: fib_arr
        integer :: i
        fib_arr(1) = 1
        fib_arr(2) = 2
        
        do i=3,n
            fib_arr(i) = fib_arr(i-1) + fib_arr(i-2)
        end do
        return
    end function
    
    function quadratic_equation(a, b, c) result (roots)
        IMPLICIT NONE
        real(kind=dp), dimension(2) :: roots
        real(kind=dp), intent(in) :: a, b, c
        real(kind=dp) :: det
        
        det = b**2 - 4 * a * c
        
        if (det < 0) then
            write(*, *) "No real solutions"
            stop
        end if
        
        det = sqrt(det)
        
        roots(1) = (-b + det) / (2 * a)
        roots(2) = (-b - det) / (2 * a)
    end function quadratic_equation
    
    pure real(kind=dp) function stirlings_approx(n)
          integer, intent(in) :: n
          stirlings_approx = n * log(real(n, kind=dp)) - n
          return
    end function stirlings_approx
    
    pure real(kind=dp) function log_n_factorial(n)
          integer, intent(in) :: n
          integer :: i
          log_n_factorial = sum((/(log(real(i, kind=dp)), i=1, n)/))
          return
    end function log_n_factorial
    
    pure logical function approx_equal(a, b)
        real(kind=dp), intent(in) :: a, b
        real(kind=dp), parameter :: atol = 1e-2_dp
        real(kind=dp), parameter :: rtol = 1e-5_dp
        approx_equal = abs(a - b) .le. (atol + (rtol * abs(b)))
        return
    end function approx_equal
    
end module mathematical