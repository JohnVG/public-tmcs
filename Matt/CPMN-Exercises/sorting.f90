module sorting
    use, intrinsic :: iso_fortran_env
    IMPLICIT NONE
    integer, parameter :: dp = REAL64
    

    public bubble_sort, shuffle, insertion_sort, quick_sort
    private partition, is_sorted, dp
contains
    pure logical function is_sorted(array)
        ! Checks if an array of integers is sorted
        ! by iterating through it and seeing if we
        ! increase each time
        integer, intent(in), dimension(:) :: array
        integer :: i
        
        do i=2, size(array)
            if (array(i-1) > array(i)) then
                is_sorted = .false.
                return
            end if
        end do
        is_sorted = .true.
        return
    end function is_sorted
    
    subroutine shuffle(array)
        ! Implements the Knuth Shuffle algorithm, to turn
        ! an ordered array into an unordered one
        integer, intent(inout), dimension(:) :: array
        integer :: i
        integer :: new_pos
        integer :: temp
        real(kind=dp):: new_rand
    
        ! Iterate from the end of the array down
        do i=size(array), 2, -1
            call random_number(new_rand)
            new_pos = int(new_rand * i) + 1
            temp = array(new_pos)
            array(new_pos) = array(i)
            array(i) = temp
        end do
    end subroutine shuffle
    
    subroutine bubble_sort(array)
        ! Bubble sort algorithm, modifying array in place.
        ! Takes n^2 operations.
        ! Only works on integer arrays.
        integer, intent(inout), dimension(:) :: array
        integer :: temp
        integer :: n, i, j

        logical :: swapped
        
        swapped = .true.
        n = size(array)
        do j= 1, size(array) - 1
            swapped = .false.
            do i=2, n
                if (array(i-1) > array(i)) then
                    ! The previous element is larger than this element,
                    ! so swap them.
                    temp = array(i)
                    array(i) = array(i-1)
                    array(i-1) = temp
                    swapped = .true.
                end if
            end do 
            ! This is an optimisation because we "float" the nth largest
            ! element to position n each iteration, so don't have to look!
            n = n - 1
        end do
        
    end subroutine bubble_sort
    
    subroutine insertion_sort(array)
        integer, intent(inout), dimension(:) :: array
        
        integer :: i, j
        integer :: temp
        
        do i=2, size(array)
            temp = array(i)
            ! Count down until we find the right index
            ! and move everything else forwards
            do j=i-1,1,-1
                if (array(j) > temp) then
                    array(j+1) = array(j)
                else
                    ! No more moving forwards.
                    exit
                end if
            end do
            array(j+1) = temp
        end do
    end subroutine
    
    recursive subroutine quick_sort(array, low, high)
        integer, dimension(:), intent(inout) :: array
        integer, intent(in) :: low, high
        integer :: pivot
        
        if (low < high) then
            pivot = partition(array, low, high)
            call quick_sort(array, low, pivot-1)
            call quick_sort(array, pivot+1, high)
        end if

    end subroutine quick_sort
    
    integer function partition(array, low, high)
        integer, dimension(:), intent(inout) :: array
        integer, intent(in) :: low, high
        integer :: pivot
        integer :: i, j
        integer :: temp
        
        i = low
        pivot = array(high)
        do j=low, high
            if (array(j) < pivot) then
                temp = array(i)
                array(i) = array(j)
                array(j) = temp
                i = i + 1
            end if
        end do
        
        temp = array(i)
        array(i) = array(high)
        array(high) = temp
        
        partition = i
        return
    end function partition
end module sorting