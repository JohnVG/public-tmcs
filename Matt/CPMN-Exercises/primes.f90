module primes
    ! Contains answers for exercises 14 and 15 of the short_projects.pdf file.
    
    IMPLICIT NONE
    PRIVATE
    PUBLIC :: sundaram, eratosthenes
contains
    pure function eratosthenes(max_num) result (primes)
        IMPLICIT NONE
        integer, intent(in) :: max_num
        logical, dimension(max_num) :: flags
        integer, allocatable, dimension(:) :: primes
        integer :: i, j
        
        flags = .true.
        do i=2,int(sqrt(real(max_num)))
            if (flags(i)) then
                do j=i*i,max_num,i
                    flags(j) = .false.
                end do
            end if
        end do
        
        
        allocate(primes(count(flags)))
        
        j = 1
        do i=1, max_num
            if (flags(i)) then
                primes(j) = i
                j = j+1
            end if
        end do
        
        return
    end function eratosthenes
    
    function sundaram(max_num) result (primes)
        IMPLICIT NONE
        integer, intent(in) :: max_num
        integer :: max_num_adj 
        logical, dimension(max_num / 2) :: flags
        integer, allocatable, dimension(:) :: primes
        integer :: i, j, index
        max_num_adj = (max_num - 1) /2
        flags = .true.
        
        do j=1,int(sqrt(real(max_num_adj)))
            do i=1, j
                index = i + j + 2 * (i*j)
                if (index < max_num_adj) then
                    flags(index) = .false.
                else
                    exit
                end if
            end do
        end do
        
        allocate(primes(count(flags)))
        
        j = 2
        primes(1) = 1
        do i=1, max_num_adj
            if (flags(i)) then
                primes(j) = 2*i + 1
                j = j+1
            end if
        end do
        
        return
    end function sundaram
end module