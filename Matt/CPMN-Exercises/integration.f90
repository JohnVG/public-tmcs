MODULE integration
    use, intrinsic :: iso_fortran_env
    IMPLICIT NONE
    integer, parameter :: dp = REAL64
    private
    public :: cube, constant_integrate, simpson_integrate, midpoint_integrate, trapezium_integrate, exact_integrate

    contains
        real(kind=dp) function cube(x)
            real(kind=dp), intent(in) :: x
            cube = x*x*x
            return
        end function cube
        
        real(kind=dp) function exact_integrate(lower, upper)
            real(kind=dp), intent(in) :: lower
            real(kind=dp), intent(in) :: upper
            exact_integrate = 0.25_dp * (upper**4) - 0.25_dp * (lower**4) 
            return
        end function exact_integrate
    
        real(kind=dp) function constant_integrate(func, lower, upper)
            real(kind=dp) :: func
            real(kind=dp), intent(in) :: lower
            real(kind=dp), intent(in) :: upper
            
            constant_integrate = (upper - lower) * func(lower)
        end function constant_integrate
        
        real(kind=dp) function midpoint_integrate(func, lower, upper)
            real(kind=dp) :: func
            real(kind=dp), intent(in) :: lower
            real(kind=dp), intent(in) :: upper
            
            midpoint_integrate = (upper - lower) * func((lower + upper)/2.0_dp)
        end function midpoint_integrate
        
        real(kind=dp) function trapezium_integrate(func, lower, upper, bins)
            real(kind=dp) :: func
            real(kind=dp), intent(in) :: lower
            real(kind=dp), intent(in) :: upper
            integer, intent(in) :: bins
            real(kind=dp) :: bin_width
            real(kind=dp), dimension(bins + 1) :: bin_x
            real(kind=dp), dimension(bins + 1) :: bin_y
            integer :: i
            
             bin_width = (upper - lower) / real(bins)
            
            do i=1, bins + 1
                bin_x(i) = real(lower) + (real(i - 1) *  bin_width)
                bin_y(i) = func(bin_x(i))
            end do
            
            trapezium_integrate = 0
            do i=1, bins
                trapezium_integrate = trapezium_integrate + (bin_y(i+1) + bin_y(i))
            end do
            trapezium_integrate = trapezium_integrate * bin_width / 2.0_dp
            
            return
        end function trapezium_integrate
        
        real(kind=dp) function simpson_integrate(func, lower, upper, bins)
            real(kind=dp) :: func
            real(kind=dp), intent(in) :: lower
            real(kind=dp), intent(in) :: upper
            integer, intent(in) :: bins
            real(kind=dp) :: bin_width
            real(kind=dp), dimension(bins + 1) :: bin_x
            real(kind=dp), dimension(bins + 1) :: bin_y
            integer :: i
            
            bin_width = (upper - lower) / real(bins)
            
            do i=1, bins + 1
                bin_x(i) = real(lower) + (real(i - 1) *  bin_width)
                bin_y(i) = func(bin_x(i))
            end do
            
            simpson_integrate = bin_y(1)
            do i=2, bins
                ! Simpsons Method uses odd and even bins added in different amounts
                ! Be careful here, as off-by-one errors can easily be introduced.
                if (mod(i, 2) == 1) then
                    simpson_integrate = simpson_integrate + (2 * bin_y(i))
                else
                    simpson_integrate = simpson_integrate + (4 * bin_y(i))
                end if
            end do
            
            simpson_integrate = simpson_integrate + bin_y(bins)
            simpson_integrate = simpson_integrate * (bin_width / 3.0_dp)
            
            return
        end function simpson_integrate
end module integration
