PROGRAM CPMN
    use sorting ! Q1, Q2, Q3
    use integration !Q4
    use temperature_conversion !Q5
    use mathematical !Q6, Q7, Q8
    use chemical ! Q9, Q10, Q11
    use rydberg ! Q12
    use partition_functions ! Q13, Q14
    use primes ! Q15, Q16
    use, intrinsic :: iso_fortran_env
    IMPLICIT NONE
    integer, parameter :: dp = REAL64
    integer, dimension(10) :: array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    real(kind=dp), parameter :: lower = 1
    real(kind=dp), parameter :: upper = 3
    real(kind=dp) :: exact_area
    integer :: i
    ! Problem 1 - Bubble Sort

    call shuffle(array) 
    write(*, *) "Before bubble sort", array    
    call bubble_sort(array)
    write(*, *) "After  bubble sort", array
    
    ! Problem 2 - Insertion Sort
    call shuffle(array) 
    write(*, *) "Before insertion sort", array   
    call insertion_sort(array)
    write(*, *) "After  insertion sort", array
    
    ! Problem 3 - Quick Sort
    call shuffle(array) 
    write(*, *) "Before quick sort", array   
    call quick_sort(array, 1, size(array))
    write(*, *) "After  quick sort", array
    
    ! Problem 4 - Integration
    exact_area = exact_integrate(lower, upper)
    write(*, *)"Exact:         ", exact_area
    write(*, *)"Constant:      ", constant_integrate(cube, lower, upper)
    write(*, *)"Midpoint:      ", midpoint_integrate(cube, lower, upper)
    write(*, *)"Trapezium 5:   ", trapezium_integrate(cube, lower, upper, 5)
    write(*, *)"Trapezium 50:  ", trapezium_integrate(cube, lower, upper, 50)
    write(*, *)"Trapezium 500: ", trapezium_integrate(cube, lower, upper, 500)
    write(*, *)"Simpson 5:     ", simpson_integrate(cube, lower, upper, 5)
    write(*, *)"Simpson 50:    ", simpson_integrate(cube, lower, upper, 50)
    write(*, *)"Simpson 500:   ", simpson_integrate(cube, lower, upper, 500)
    
    ! Exercise 5 - Temperature Conversion
    write(6, "(F9.2)") (celsius_to_fahrenheit(real(i, kind=dp)) , i=0, 100)
    
    ! Exercise 6 - Quadratic Equation
    write(6, *) (quadratic_equation(real(2, kind=dp), real(5, kind=dp), real(2, kind=dp)))
    
    ! Exercise 7 - Stirling's Approximation
    write(*, *) log_n_factorial(10), stirlings_approx(10)
    write(*, *) log_n_factorial(100), stirlings_approx(100)
    write(*, *) log_n_factorial(1000), stirlings_approx(1000)
    write(*, *) log_n_factorial(5000), stirlings_approx(5000)
    
    ! Exercise 8 - Fibonacci
    write(*, *) fibonacci(25)
    
    ! Exercise 9 - CO2 from 0.1 bar to 1bar
    write(*, *) (van_der_waals(real(i, kind=dp) * 1e4_dp, 0.0247_dp, 26.6e-6_dp, 298.0_dp, 1.0_dp), i=1, 11)
    write(*, *) (van_der_waals(real(i, kind=dp) * 1e4_dp, 0.366_dp, 42.9e-6_dp, 298.0_dp, 1.0_dp), i=1, 11)
    
    ! Exercise 10 - Mass Spectrometry Unknowns
    write(*, *) mass_spec_unknowns(103.08_dp)
    write(*, *) mass_spec_unknowns(152.11_dp)
    write(*, *) mass_spec_unknowns(152.10_dp)
    write(*, *) mass_spec_unknowns(153.09_dp)
    
    ! Exercise 11 - Hydrogenation Rate
    write(*, *) (hydrogenation_rate(1.0_dp, 1.0_dp, real(i, kind=dp)), i=0, 20)
    
    ! Exercise 12 - Rydberg
    write(*, *) rydberg_equation(9953.50_dp)
    write(*, *) rydberg_equation(5503.80_dp)
    write(*, *) rydberg_equation(1341.23_dp)
    write(*, *) rydberg_equation(359.87_dp)
    
    ! Exercise 12a - Rydberg in common
    write(*, *) rydberg_common([15241.28_dp, 20575.725_dp, 23044.812_dp,  24386.04_dp, 25194.76_dp])
    
    ! Exercise 13 - Partition Functions
    write(*, *) "Partition Function", (rotational_partition(15.2_dp, real(10*i + 5, kind=dp)) ,i=0, 15)
    write(*, *) "Partition Function", (rotational_partition_approx(15.2_dp, real(10*i + 5, kind=dp)) ,i=0, 15)
    
    write(*, *) (rotational_partition(85.4_dp, real(10*i + 5, kind=dp)) ,i=0, 15)
    write(*, *) (rotational_partition_approx(85.4_dp, real(10*i + 5, kind=dp)) ,i=0, 15)
    
    write(*, *) (rotational_partition(2.9_dp, real(10*i + 5, kind=dp)) ,i=0, 15)
    write(*, *) (rotational_partition_approx(2.9_dp, real(10*i + 5, kind=dp)) ,i=0, 15)
    
    ! Exercise 14 - Rotational Spectrum
    write(*, *) rotational_spectrum(15.2_dp, 298.0_dp)
    
    ! Exercise 15 - Primes
    write(*, *) eratosthenes(50)
    write(*, *) eratosthenes(500)
    
    ! Exercise 16 - Different Primes
    write(*, *) sundaram(50)
    write(*, *) sundaram(500)

END program CPMN
