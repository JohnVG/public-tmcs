module rydberg
    ! Contains answers for Q6, Q7 and Q8 of short_projects.pdf
    use, intrinsic :: iso_fortran_env
    use mathematical, only: approx_equal
    IMPLICIT NONE
    integer, parameter :: dp = REAL64
    real(kind=dp), parameter :: R_H = 109737.2 ! cm^-1
    PRIVATE
    PUBLIC :: rydberg_equation, rydberg_common

contains
    pure function rydberg_equation(freq) result (n_values)
        ! Determines n1 and n2 for a given frequency by
        ! sheer brute force.
        ! Returns -1 if none are found.
        IMPLICIT NONE
        integer, dimension(2) :: n_values
        real(kind=dp), intent(in) :: freq
        integer, parameter :: max_n = 10
        integer :: n_1, n_2
        real(kind=dp) :: inv_n_1_sq, inv_n_2_sq
        
        do n_1=max_n,1, -1
            inv_n_1_sq = 1.0_dp / real(n_1**2, kind=dp)
            do n_2=max_n,n_1+1,-1
                inv_n_2_sq = 1.0_dp / real(n_2**2, kind=dp)
                if (approx_equal(R_H * (inv_n_1_sq - inv_n_2_sq), freq)) then
                    n_values = [n_1, n_2]
                    return
                end if
            end do
        end do
        n_values = [-1, -1]
    end function rydberg_equation
    
    pure integer function rydberg_common(freq_arr)
        IMPLICIT NONE
        real(kind=dp), dimension(:), intent(in) :: freq_arr
        integer, dimension(size(freq_arr), 2) :: n_values
        integer :: i
        do i=1, size(freq_arr)
            n_values(i, :) = rydberg_equation(freq_arr(i))
        end do
        
        if (ALL(n_values(:, 1) == n_values(1, 1))) then
            ! It's a from match
            rydberg_common = n_values(1, 1)
            return
        end if
        
        if (ALL(n_values(:, 2) == n_values(1, 2))) then
             ! It's a to match
            rydberg_common = n_values(1, 2)
            return                
        end if

        rydberg_common = -1
    end function rydberg_common
end module rydberg