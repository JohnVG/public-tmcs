module partition_functions
    use, intrinsic :: iso_fortran_env
    use mathematical, only: approx_equal
    IMPLICIT NONE
    integer, parameter :: dp = REAL64
    real(kind=dp), parameter :: boltz = 0.69503476_dp
    PRIVATE
    PUBLIC :: rotational_partition, rotational_partition_approx, rotational_spectrum
contains
    pure real(kind=dp) function rotational_partition(B, temp)
        IMPLICIT NONE
        real(kind=dp), intent(in) :: B, temp

        integer :: j
        real(kind=dp) :: last_q, current_q, exp_term
        last_q = -100.0_dp
        current_q = 0.0_dp
        j = 0
        do while(.not.approx_equal(last_q, current_q))
            exp_term = -1.0_dp *  B*j * (j+1) / (boltz * temp)
            last_q = current_q
            current_q = last_q + ((2.0_dp * j + 1) * exp(exp_term))
            j = j + 1
        end do
        rotational_partition = current_q
        return
    end function rotational_partition
    
    pure real(kind=dp) function rotational_partition_approx(B, temp)
        IMPLICIT NONE
        real(kind=dp), intent(in) :: B, temp
        rotational_partition_approx = (boltz * temp) / B
        return
    end function rotational_partition_approx
    
    pure function rotational_spectrum(B, temp) result(frequencies)
        IMPLICIT NONE
        integer, parameter :: max_branches = 10
        
        real(kind=dp), intent(in) :: B, temp
        real(kind=dp), dimension(max_branches * 2, 2) :: frequencies
        

        integer :: j
        real(kind=dp) :: exp_term
        
        do j=0, max_branches - 1
            exp_term = -1.0_dp *  B*J * (J+1.0_dp) / (boltz * temp)
            frequencies(j+1, 1) = 2.0_dp * B * (J+1.0_dp)
            frequencies(j+1, 2) = (2.0_dp * J + 1.0_dp) * exp(exp_term)
        end do
        
        do j=1, max_branches
            exp_term = -1.0_dp *  B*J * (J+1.0_dp) / (boltz * temp)
            frequencies(j + max_branches, 1) = -2.0_dp * B * J
            frequencies(j + max_branches, 2) = (2.0_dp * J + 1.0_dp) * exp(exp_term)
        end do
        
        return
    end function rotational_spectrum
end module partition_functions
